package araujo.felipe.desafio.android.service;

import org.androidannotations.annotations.rest.Accept;
import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Rest;
import org.androidannotations.api.rest.MediaType;
import org.springframework.http.converter.json.GsonHttpMessageConverter;

import araujo.felipe.desafio.android.engine.Feed;
import araujo.felipe.desafio.android.util.Constants;

@Rest(rootUrl = Constants.URL_MASTER, converters = {GsonHttpMessageConverter.class})
public interface RESTService {

    @Get(Constants.END_POINT_UPDATE_FEED + "{page}")
    @Accept(MediaType.APPLICATION_JSON)
    Feed searchFeedByPage(Integer page);

}
