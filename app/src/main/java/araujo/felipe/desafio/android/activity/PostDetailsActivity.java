package araujo.felipe.desafio.android.activity;

import android.graphics.Bitmap;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import araujo.felipe.desafio.android.R;
import araujo.felipe.desafio.android.engine.Shot;
import de.hdodenhof.circleimageview.CircleImageView;

@EActivity(R.layout.activity_post_details)
public class PostDetailsActivity extends ActionBarActivity {

    @Extra("SHOT")
    Shot shot;

    @ViewById(R.id.ivPreviewImage)
    ImageView ivPreviewImage;
    @ViewById(R.id.tvImageTitle)
    TextView tvImageTitle;
    @ViewById(R.id.tvNumberVisualizations)
    TextView tvNumberVisualizations;
    @ViewById(R.id.tvNumberComents)
    TextView tvNumberComents;
    @ViewById(R.id.tvNumberLoves)
    TextView tvNumberLoves;
    @ViewById(R.id.linearInfos)
    LinearLayout linearInfos;
    @ViewById(R.id.tvComentary)
    TextView tvComentary;
    @ViewById(R.id.tvName)
    TextView tvName;
    @ViewById(R.id.tvUsername)
    TextView tvUsername;
    @ViewById(R.id.tvTwitter)
    TextView tvTwitter;
    @ViewById(R.id.tvSite)
    TextView tvSite;
    @ViewById(R.id.ivProfile)
    CircleImageView ivProfile;

    private DisplayImageOptions options;
    private ImageLoadingListener animateFirstListener;


    @AfterViews
    void afterViews(){

        setTitle(shot.getPlayer().getName());

        initConfigImageLoader();
        animateFirstListener = new AnimateFirstDisplayListener();

        ImageLoader.getInstance().displayImage(shot.getImage400Url(), ivPreviewImage, options, animateFirstListener);

        tvImageTitle.setText(shot.getTitle());
        tvNumberVisualizations.setText(String.valueOf(shot.getViewsCount()));
        tvNumberLoves.setText(String.valueOf(shot.getLikesCount()));
        tvNumberComents.setText(String.valueOf(shot.getCommentsCount()));

        tvComentary.setText(Html.fromHtml(shot.getDescription()));
        tvName.setText(shot.getPlayer().getName());
        tvUsername.setText("User : " + shot.getPlayer().getUsername());
        tvTwitter.setText("Twitter : @" + shot.getPlayer().getTwitterScreenName());
        tvSite.setText("Site : " + shot.getPlayer().getWebsiteUrl());

        ViewCompat.setTransitionName(ivPreviewImage, "photo");
        ViewCompat.setTransitionName(tvImageTitle, "title");
        ViewCompat.setTransitionName(linearInfos, "linear");

        updateImage();
    }

    @UiThread(delay = 500)
    void updateImage(){
        ImageLoader.getInstance().displayImage(shot.getImageUrl(), ivPreviewImage, options, animateFirstListener);
        ImageLoader.getInstance().displayImage(shot.getPlayer().getAvatarUrl(), ivProfile, options, animateFirstListener);
    }


    private void initConfigImageLoader(){
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
    }

    private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }

}
