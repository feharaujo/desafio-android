package araujo.felipe.desafio.android.engine;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Player implements Serializable {

    @SerializedName("id")
    private Integer id;
    @SerializedName("name")
    private String name;
    @SerializedName("location")
    private String location;
    @SerializedName("followers_count")
    private Integer followersCount;
    @SerializedName("draftees_count")
    private Integer drafteesCount;
    @SerializedName("likes_count")
    private Integer likesCount;
    @SerializedName("likes_received_count")
    private Integer likesReceivedCount;
    @SerializedName("comments_count")
    private Integer commentsCount;
    @SerializedName("comments_received_count")
    private Integer commentsReceivedCount;
    @SerializedName("rebounds_count")
    private Integer reboundsCount;
    @SerializedName("rebounds_received_count")
    private Integer reboundsReceivedCount;
    @SerializedName("url")
    private String url;
    @SerializedName("avatar_url")
    private String avatarUrl;
    @SerializedName("username")
    private String username;
    @SerializedName("twitter_screen_name")
    private String twitterScreenName;
    @SerializedName("website_url")
    private String websiteUrl;
    @SerializedName("drafted_by_player_id")
    private Integer draftedByPlayerId;
    @SerializedName("shots_count")
    private Integer shotsCount;
    @SerializedName("following_count")
    private Integer followingCount;
    @SerializedName("created_at")
    private String createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getFollowersCount() {
        return followersCount;
    }

    public void setFollowersCount(Integer followersCount) {
        this.followersCount = followersCount;
    }

    public Integer getDrafteesCount() {
        return drafteesCount;
    }

    public void setDrafteesCount(Integer drafteesCount) {
        this.drafteesCount = drafteesCount;
    }

    public Integer getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(Integer likesCount) {
        this.likesCount = likesCount;
    }

    public Integer getLikesReceivedCount() {
        return likesReceivedCount;
    }

    public void setLikesReceivedCount(Integer likesReceivedCount) {
        this.likesReceivedCount = likesReceivedCount;
    }

    public Integer getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(Integer commentsCount) {
        this.commentsCount = commentsCount;
    }

    public Integer getCommentsReceivedCount() {
        return commentsReceivedCount;
    }

    public void setCommentsReceivedCount(Integer commentsReceivedCount) {
        this.commentsReceivedCount = commentsReceivedCount;
    }

    public Integer getReboundsCount() {
        return reboundsCount;
    }

    public void setReboundsCount(Integer reboundsCount) {
        this.reboundsCount = reboundsCount;
    }

    public Integer getReboundsReceivedCount() {
        return reboundsReceivedCount;
    }

    public void setReboundsReceivedCount(Integer reboundsReceivedCount) {
        this.reboundsReceivedCount = reboundsReceivedCount;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTwitterScreenName() {
        return twitterScreenName;
    }

    public void setTwitterScreenName(String twitterScreenName) {
        this.twitterScreenName = twitterScreenName;
    }

    public String getWebsiteUrl() {
        return websiteUrl;
    }

    public void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    public Integer getDraftedByPlayerId() {
        return draftedByPlayerId;
    }

    public void setDraftedByPlayerId(Integer draftedByPlayerId) {
        this.draftedByPlayerId = draftedByPlayerId;
    }

    public Integer getShotsCount() {
        return shotsCount;
    }

    public void setShotsCount(Integer shotsCount) {
        this.shotsCount = shotsCount;
    }

    public Integer getFollowingCount() {
        return followingCount;
    }

    public void setFollowingCount(Integer followingCount) {
        this.followingCount = followingCount;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
