package araujo.felipe.desafio.android.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import araujo.felipe.desafio.android.R;
import araujo.felipe.desafio.android.engine.Shot;
import araujo.felipe.desafio.android.util.Constants;

public class PostAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<Shot> itens;
    private DisplayImageOptions options;
    private ImageLoadingListener animateFirstListener;


    public PostAdapter(Activity context, List<Shot> itens){
        this.itens = itens;
        mInflater = LayoutInflater.from(context);

        animateFirstListener = new AnimateFirstDisplayListener();

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.dribbble)
                .showImageOnFail(R.drawable.ic_error_red_48dp)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();

    }

    @Override
    public int getCount() {
        return itens.size();
    }

    @Override
    public Shot getItem(int position) {
        return itens.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        convertView = mInflater.inflate(R.layout.item_shot, null);
        holder = new ViewHolder();
        holder.tvTitle = (TextView) convertView.findViewById(R.id.tvImageTitle);
        holder.tvNumberComents = (TextView) convertView.findViewById(R.id.tvNumberComents);
        holder.tvNumberLoves = (TextView) convertView.findViewById(R.id.tvNumberLoves);
        holder.tvNumberVisualizations = (TextView) convertView.findViewById(R.id.tvNumberVisualizations);
        holder.linearInfos = (LinearLayout) convertView.findViewById(R.id.linearInfos);
        holder.previewImage = (ImageView) convertView.findViewById(R.id.ivPreviewImage);

        // transparencia
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            Drawable c = holder.linearInfos.getBackground();
            c.setAlpha(Math.round(Constants.TRANSPARENCY_RATE * 255));
            holder.linearInfos.setBackground(c);
        }

        final Shot item = itens.get(position);

        holder.tvTitle.setText(item.getTitle());
        holder.tvNumberComents.setText(String.valueOf(item.getCommentsCount()));
        holder.tvNumberLoves.setText(String.valueOf(item.getLikesCount()));
        holder.tvNumberVisualizations.setText(String.valueOf(item.getViewsCount()));

        ImageLoader.getInstance().displayImage(item.getImageTeaserUrl(), holder.previewImage, options, animateFirstListener);

        return convertView;
    }

    public void addItems(List<Shot> newItems) {
        if (null == newItems || newItems.size() <= 0) {
            return;
        }

        if (itens == null) {
            itens = new ArrayList<>();
        }

        itens.addAll(newItems);
        notifyDataSetChanged();
    }


    private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }

    private static class ViewHolder {
        ImageView image;
        TextView tvTitle;
        TextView tvNumberComents;
        TextView tvNumberLoves;
        TextView tvNumberVisualizations;
        LinearLayout linearInfos;
        ImageView previewImage;
    }
}
