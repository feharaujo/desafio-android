package araujo.felipe.desafio.android.activity;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.jafarkhq.views.EndlessListView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NonConfigurationInstance;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import araujo.felipe.desafio.android.R;
import araujo.felipe.desafio.android.adapter.PostAdapter;
import araujo.felipe.desafio.android.controller.MainController;
import araujo.felipe.desafio.android.engine.Feed;
import araujo.felipe.desafio.android.engine.Shot;
import araujo.felipe.desafio.android.util.ConnectionUtil;

@EActivity(R.layout.activity_main)
public class MainActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {

    @Bean
    @NonConfigurationInstance
    MainController bean;

    @ViewById(R.id.shimmer_view_container)
    ShimmerFrameLayout shimmerViewContainer;
    @ViewById(R.id.lvPosts)
    EndlessListView lvPosts;
    @ViewById(R.id.linearLoading)
    LinearLayout linearLoading;
    @ViewById(R.id.btnReload)
    ImageButton btnReload;
    @ViewById(R.id.tvMsgErro)
    TextView tvMsgErro;

    PostAdapter adapter;
    private List<Shot> mShots;
    private Integer mContPage = 0;

    @AfterViews
    void afterViews(){
        mShots = new ArrayList<>();
        mContPage++;
        bean.firstFeed(mContPage);
    }

    @Override
    public void onResume() {
        super.onResume();

        shimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        stopShimmerAnimation();
        super.onPause();
    }

    public void firstServiceResponse(Feed feed){
        mShots.addAll(feed.getShots());
        adapter = new PostAdapter(this, mShots);

        lvPosts.setAdapter(adapter);
        lvPosts.setOnItemClickListener(this);
        lvPosts.setOnLoadMoreListener(loadMoreListener);

        stopShimmerAnimation();
        linearLoading.setVisibility(View.INVISIBLE);
        lvPosts.setVisibility(View.VISIBLE);
        btnReload.setVisibility(View.VISIBLE);
    }

    // recebe os itens atualizados
    public void updateFeed(Feed feed){
        mShots.addAll(feed.getShots());
        adapter.addItems(feed.getShots());
        lvPosts.loadMoreCompleat();

        tvMsgErro.setVisibility(View.INVISIBLE);
        lvPosts.setVisibility(View.VISIBLE);

    }

    public void errorServiceResponse(){

    }

    public void stopShimmerAnimation(){
        shimmerViewContainer.stopShimmerAnimation();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        Intent intent = new Intent(getApplicationContext(), PostDetailsActivity_.class);
        intent.putExtra("SHOT", mShots.get(position));

        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this,
                Pair.create(v.findViewById(R.id.ivPreviewImage), "photo"),
                Pair.create(v.findViewById(R.id.tvImageTitle), "title"),
                Pair.create(v.findViewById(R.id.linearInfos), "linear"));
        ActivityCompat.startActivity(this, intent, options.toBundle());
    }

    private EndlessListView.OnLoadMoreListener loadMoreListener = new EndlessListView.OnLoadMoreListener() {

        @Override
        public boolean onLoadMore() {
            mContPage++;
            bean.updateFeed(mContPage);

            return true;
        }
    };

    @Click(R.id.btnReload)
    void reload(){
        if(!ConnectionUtil.isOnline(this)){
            showErrorUpdateFeed();
            return;
        }

        mContPage = 0;
        adapter = null;
        mShots.clear();

        adapter = null;
        adapter = new PostAdapter(this, new ArrayList<Shot>());
        lvPosts.setAdapter(adapter);

        //mContPage++;
        bean.updateFeed(mContPage);
    }

    public void showErrorMessage(){
        tvMsgErro.setVisibility(View.VISIBLE);
        stopShimmerAnimation();
        linearLoading.setVisibility(View.INVISIBLE);
        lvPosts.setVisibility(View.INVISIBLE);
        btnReload.setVisibility(View.VISIBLE);
    }

    public void showErrorUpdateFeed(){
        Toast.makeText(this, getString(R.string.msg_error_connection_toast), Toast.LENGTH_SHORT).show();
    }

}
