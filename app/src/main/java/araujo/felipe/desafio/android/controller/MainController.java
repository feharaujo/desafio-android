package araujo.felipe.desafio.android.controller;

import android.util.Log;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.rest.RestService;

import araujo.felipe.desafio.android.activity.MainActivity;
import araujo.felipe.desafio.android.engine.Feed;
import araujo.felipe.desafio.android.service.RESTService;

@EBean
public class MainController {

    @RootContext
    MainActivity context;

    @RestService
    RESTService service;

    /**
     * Primeira chamada possui um pequeno delay para mostrar logo inicial
     * @param page
     */
    @Background(delay = 1500)
    public void firstFeed(Integer page){
        Log.v("----", "PRIMEIRA CHAMADA!");
        try {
            Feed feed = service.searchFeedByPage(page);
            if(feed == null){
                errorService();
            } else {
                firstSuccessResponse(feed);
            }
        } catch (Exception e){
            errorService();
        }
    }

    @UiThread
    void firstSuccessResponse(Feed feed){
        context.firstServiceResponse(feed);
    }

    @Background
    public void updateFeed(Integer page){
        Log.v("----", "CHAMADA " + page);
        try {
            Feed feed = service.searchFeedByPage(page);
            if(feed == null){
                errorService();
            } else {
                successResponse(feed);
            }
        } catch (Exception e){
            errorServiceUpdate();
        }
    }

    @UiThread
    void successResponse(Feed feed){
        context.updateFeed(feed);
    }

    @UiThread
    void errorService(){
        context.showErrorMessage();
    }

    @UiThread
    void errorServiceUpdate(){
        context.showErrorUpdateFeed();
    }


}
