package araujo.felipe.desafio.android.util;

public class Constants {

    public static final String URL_MASTER = "http://api.dribbble.com";
    public static final String END_POINT_UPDATE_FEED = "/shots/popular?page=";

    public static final float TRANSPARENCY_RATE = 0.5f;

}
